package main

import (
	"fmt"
	"bufio"
	"os"
)

/*
* Panic : Forma en la que nosotros podemos imprimir un error.
* Muestra el stack, más detallado el  error como la linea, etc.
* Lo que realmente hace es paniquiar el sistema.
*************************************************
* Recover: Es una forma para detener un Panic.
* 
*/
func main() {
	executeReadFile()	
	fmt.Println("Nunca se Imprime?")
}

func executeReadFile() {

	flag:= read_file()
	fmt.Println(flag)
}
func read_file() bool{
	file,err := os.Open("./holaa.txt")
	//defer file.Close() //Defer sive para que esta función se ejecuta, cuando suceda un return en cualquier parte de esta funcion

	defer func () { //Con esto garantizamos que el archivo se cierre sin importar en que parte retorne la función
		file.Close()
		fmt.Println("Se cerro el archivo con Defer")
		r:=recover()
		fmt.Println(r)
	}()


	if err!=nil {
		panic(err)
		
	}
	scanner:= bufio.NewScanner(file)
	var i int
	for scanner.Scan(){
		i++
		line:= scanner.Text()
		fmt.Printf("You are in line: %d ", i)
		fmt.Println("Data is: "+line)
	}

	
	
	//file.Close()//closed file That have is  to oppened
	return true
}