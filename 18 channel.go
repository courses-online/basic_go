package main
import "fmt"


func main(){
	//Definimos un canal y el tipo

 	channel :=make (chan string)

 	//@params
 	//1. el canal(nombre o identificador del canal)
 	//2. La definicion del canal
 	//3. EL tipo de información qe se envía en ese canal

 	go func (channel chan string) {
 		for{
 			var name string
 			fmt.Scanln(&name)

 			//Enviando datos al canal
 			channel <- name
 		}
 	}(channel)

 	for{
 		//Recibir la info del canal
	 	msg := <- channel

	 	fmt.Println("Estoy imprimiendo lo que recibi del canal: "+msg)

 	}

 	//Cuando esperamos información de n canal, la rutina en la que se esta trabajando
 	//se espera hasta recibir la información.
 	//Luego continua la ejecucíon del programa.



}

