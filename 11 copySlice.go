package main
import (
        "fmt"
)

func main(){
        /*
La funcion copy,
copia el minimo de los elemntos de los dos arreglos.
osea si copia un arreglo de 5 a otro de tamaño 0.
NOTA esto sucede debido a que  tenemos un arrelo de tamaño estatico.

SOLUCION:
Crear dos slices con tamaños dinamicos

OJO
	*/
	slice :=[]int{1,2,3,4,5}
	copia:= make([]int,len(slice), cap(slice)*2)
	//Hacerr copia de sclices copy(copia,fuente)

      	copy(copia,slice)

        fmt.Println(slice)
	fmt.Println(copia)



}

