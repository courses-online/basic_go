package main

import (
	"fmt"
)

type User interface{
	Permissions() int // 1-5 Level 
	Name() string
}

//Part Admin
type Admin struct{
	name string

}

func (this Admin) Permissions()int {
	return 5
}

func (this Admin) Name()string {
	
	return this.name
}



//Second Part with other user
type Editor struct{
	name string

}
func (this Editor) Permissions()int {
	return 4
}

func (this Editor) Name()string {
	
	return this.name
}


//Authentication simulate
func auth(user User) string{
	permission := user.Permissions()
	if permission >4 {
		return user.Name()+ " Tiene Permisos de Admin"

	}else if permission > 3 {
			return user.Name()+" Tiene Permissions de Editor"
	}
	return ""
}

func main(){
	admin := Admin{"Luis Fernando"}
	editor := Editor{"Junior García"}

	//NOTA: Aunque auth, espera un parametro User, Pero le paso un admin.
	// es por que la Estructura Admin esta implementando la Interface de User
	// por lo tanto es perfectamente válido.

	fmt.Println(auth(admin))
	fmt.Println(auth(editor))

	//Otra forma de  pasar usuarios
	users := []User{Admin{"Alex Fernando"},Editor{"Mathias García"}}

	for _,user := range users{
		fmt.Println(auth(user))	
	}
	

}