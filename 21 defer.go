package main

import (
	"fmt"
	"bufio"
	"os"
)
func main() {
	flag:= read_file()
	fmt.Println(flag)
	
	
}

func read_file_with_errors_example() bool{
	file,err := os.Open("./hola.txt")

	if err!=nil {

		fmt.Println("No found file")
		
	}
	scanner:= bufio.NewScanner(file)
	var i int
	for scanner.Scan(){
		i++
		line:= scanner.Text()
		fmt.Printf("You are in line: %d ", i)
		fmt.Println("Data is: "+line)
	}

	if true {
		return true
	}

	fmt.Println("Ejecucuata?")
	file.Close()//closed file That have is  to oppened
	return true
}

//Cerrando el Archivo, Usando Defer.
func read_file() bool{
	file,err := os.Open("./hola.txt")
	//defer file.Close() //Defer sive para que esta función se ejecuta, cuando suceda un return en cualquier parte de esta funcion

	defer func () { //Con esto garantizamos que el archivo se cierre sin importar en que parte retorne la función
		file.Close()
		fmt.Println("Se cerro el archivo con Defer")	
	}()


	if err!=nil {

		fmt.Println("No found file")
		
	}
	scanner:= bufio.NewScanner(file)
	var i int
	for scanner.Scan(){
		i++
		line:= scanner.Text()
		fmt.Printf("You are in line: %d ", i)
		fmt.Println("Data is: "+line)
	}

	
	
	//file.Close()//closed file That have is  to oppened
	return true
}