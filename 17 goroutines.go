package main

import (
	"fmt"
	"time"

	"strings"
)


func main(){
	//Con el Keyword go, es para usar una goroutine
	go my_name_lentoooo("Luis Fernando")
	fmt.Println("Quee Aburridoooo")
	var wait string
	fmt.Scanln(&wait)	
	//Example function anonymou
	/*go func () {
		var wait string
		fmt.Scanln(&wait)	
	}()*/
}

func my_name_lentoooo(name string) {

	letters := strings.Split(name,"");
	for _,letter:= range(letters) {
		time.Sleep(1000 * time.Millisecond)
		fmt.Println(letter)
		
	}
	
}