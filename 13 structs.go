package main

import (
	"fmt"
)


	/*
		1. Estructura forma en que definimos nuestro tipo de dato especifico	
	*/

type Person struct{
	edad int
	telephone string
	nombre, apellido string

}

func main(){


	luis := Person{ nombre: "Luis", apellido: "Garcia"}
	mathias := Person{ 0, "322123", "Mathias", "Garcia"}
	
	fmt.Println(luis)
	fmt.Println(mathias)



	lady := new(Person)
	lady.nombre="Lady"
	
	fmt.Println((*lady).nombre)
		
	
}
