package dummy

var hello string

//Atributos Exportados o ATTIBUT PUBLIC
var Name string



func init() {
	
	hello="Hola, Luis"
	Name ="Mathias"

}

//Si la letra inicial del nombre de la funcion esta en mayuscula, quiere decir 	que es PUBLICA
func HelloWorld() string{

	return hello
	
}

//Si la letra inicial del nombre de la funcion esta en miniscula, quiere decir 	que es una FUncion PRIVADA
func hola() string{

	return "Hello Hello World"
	
}