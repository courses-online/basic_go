package main

import (
	"net/http"
	"encoding/json"
)

//La letra inicial de un attributo en MAyus, significa que son Publicos
//La letra inicial de un attributo en Minus, significa que son privados
//Esto en las Structuras que deseamos enviar al navegador

type Course struct{
	Title string `json:"title"`
	NumeroVideos int
}

//Exportar un arreglo de Cursos
type Courses []Course

func main() {

	http.HandleFunc("/",func( w http.ResponseWriter, r  *http.Request){
		courseList := Courses{
			Course{"Curso de Go",30},
			Course{"Curso de Go",20},
			Course{"Curso de Go",40},
			Course{"Curso de Go",50},
			Course{"Curso de Go",60},
			}
		json.NewEncoder(w).Encode(courseList)
	})
	http.ListenAndServe(":8001",nil)
	
}