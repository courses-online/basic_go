package main
//Se usa Scanf para leer y tambien usamos
// bufio, como otra alternativa de leer 
import (
	"fmt"
	"bufio"
	"os"
)

func main (){
	precio :=1.2
	

	fmt.Printf("Mi edad es %f \n", precio)
	
	var edad int
	var name string

	fmt.Println("Coloca tu edad")
	fmt.Scanf("%d\n",&edad)
	fmt.Println("Coloca tu nombre")
	fmt.Scanf("%s\n",&name)
	fmt.Printf(" Mi nombre es %s\n",name)
	fmt.Printf("Mi edad es %d \n", edad)

	reader:= bufio.NewReader(os.Stdin)

	fmt.Println("Ingresa tu nombre")
	nombre,err := reader.ReadString('\n')
	
	if err != nil {

		fmt.Println(err)
	}else{

		fmt.Println("Hola "+nombre)
	}	

}
