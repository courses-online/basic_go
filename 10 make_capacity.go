package main
import (
        "fmt"
)

func main(){
	//Si el append rebasa la capacidad del sclice, entonces este crea un nuevo slice 
//para insertar este uevo elemento.

//Pero en el caso que no rebase la capacidad el sclice, entonces simplemente lo inserta       
        slice := make([]int,0,4)
	slice=append(slice,2)
        fmt.Println(slice)


}
