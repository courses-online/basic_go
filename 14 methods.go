package main

import (
	"fmt"
)


	/*
		1. Estructura forma en que definimos nuestro tipo de dato especifico	
	*/

type Person struct{
	age int
	telephone string
	first_name string
	last_name string

}

func (this Person) full_name() string{
	return this.first_name + " " + this.last_name
}

func (this *Person) set_first_name(first_name string){

	this.first_name=first_name
	
	
}

func main(){

	lady := new(Person)

	lady.first_name = "Lady"

	lady.set_first_name("Luis")

	lady.last_name = "Garcia"
	
	fmt.Println(lady.full_name())
}


